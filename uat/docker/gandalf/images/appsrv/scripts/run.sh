#!/bin/bash
#
# Script to start gandalf server
#

echo "Starting Gandalf....."

java -jar -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5005 /var/lib/springboot/gandalf-1.0.0-SNAPSHOT.jar --spring.profiles.active=uat

