#!/bin/bash
#
# Master script to build a project
#
set -e
set -x

# Check environment
if [ -z $UAT_HOME ]; then
  echo "ERROR: UAT_HOME not set. This is the home directory of this project."
  exit 1
fi
if [ -z $UAT_TAG ]; then
  echo "ERROR: UAT_TAG not set."
  exit 1
fi
if [ -z $PRJ_NAME ]; then
  echo "ERROR: PRJ_NAME not set. Provide the project name here."
  exit 1
fi
if [ -z $BB_USER ]; then
  echo "ERROR: BB_USER not set. Provide the BitBucket account name here."
  exit 1
fi
if [ -z $BRANCH ]; then
  echo "INFO: BRANCH not set. Will use the master branch."
fi
if [ -z $APP_SRV_PORT ]; then
  echo "ERROR: APP_SRV_PORT not set."
  exit 1
fi

if [ -z $BRANCH ]; then
  UAT_INSTANCE_ID="${PRJ_NAME}_${UAT_TAG}"
else
  UAT_INSTANCE_ID="${PRJ_NAME}_${BRANCH}_${UAT_TAG}"
fi

# Check instance if its not running
#TODO: Check from docker 
echo "---------------------------------------------------------"
echo "Starting instance $UAT_INSTANCE_ID ......."
echo "---------------------------------------------------------"

# Setup build script names
CLONE_SCRIPT_NAME=clone_project.sh
CONFIGURE_SCRIPT_NAME=configure_project.sh
BUILD_SCRIPT_NAME=build_project.sh
DOCKER_BUILD_SCRIPT_NAME=docker_build.sh
START_SERVICE_SCRIPT_NAME=start.sh

# Set "Common" scripts as the default build scripts
CLONE_SCRIPT=$UAT_HOME/scripts/common/$CLONE_SCRIPT_NAME
CONFIGURE_SCRIPT=$UAT_HOME/scripts/common/$CONFIGURE_SCRIPT_NAME
BUILD_SCRIPT=$UAT_HOME/scripts/common/$BUILD_SCRIPT_NAME
DOCKER_BUILD_SCRIPT=$UAT_HOME/scripts/common/$DOCKER_BUILD_SCRIPT_NAME
START_SERVICE_SCRIPT=$UAT_HOME/scripts/common/$START_SERVICE_SCRIPT_NAME

#
# Override build scripts with project specific scripts if they exist.
#
PRJ_SCRIPTS_DIR="$UAT_HOME/scripts/projects/${PRJ_NAME}"
if [ -f "${PRJ_SCRIPTS_DIR}/${CLONE_SCRIPT_NAME}" ]; then
  CLONE_SCRIPT="${PRJ_SCRIPTS_DIR}/${CLONE_SCRIPT_NAME}"
fi
if [ -f "${PRJ_SCRIPTS_DIR}/${CONFIGURE_SCRIPT_NAME}" ]; then
  CONFIGURE_SCRIPT="${PRJ_SCRIPTS_DIR}/${CONFIGURE_SCRIPT_NAME}"
fi
if [ -f "${PRJ_SCRIPTS_DIR}/${BUILD_SCRIPT_NAME}" ]; then
  BUILD_SCRIPT="${PRJ_SCRIPTS_DIR}/${BUILD_SCRIPT_NAME}"
fi
if [ -f "${PRJ_SCRIPTS_DIR}/${DOCKER_BUILD_SCRIPT_NAME}" ]; then
  DOCKER_BUILD_SCRIPT="${PRJ_SCRIPTS_DIR}/${DOCKER_BUILD_SCRIPT_NAME}"
fi
if [ -f "${PRJ_SCRIPTS_DIR}/${START_SERVICE_SCRIPT_NAME}" ]; then
  START_SERVICE_SCRIPT="${PRJ_SCRIPTS_DIR}/${START_SERVICE_SCRIPT_NAME}"
fi

echo "CLONE_SCRIPT         : ${CLONE_SCRIPT}"
echo "CONFIGURE_SCRIPT     : ${CONFIGURE_SCRIPT}"
echo "BUILD_SCRIPT         : ${BUILD_SCRIPT}"
echo "DOCKER_BUILD_SCRIPT  : ${DOCKER_BUILD_SCRIPT}"
echo "START_SERVICE_SCRIPT : ${START_SERVICE_SCRIPT}"

export COMPOSE_PROJECT_NAME=$UAT_INSTANCE_ID

${CLONE_SCRIPT} "${BB_USER}" "${PRJ_NAME}" "${BRANCH}"
if [ -f  "${CONFIGURE_SCRIPT}" ]; then
  ${CONFIGURE_SCRIPT}
else
  echo "Skipping configure step."
fi
${BUILD_SCRIPT}
${DOCKER_BUILD_SCRIPT}
${START_SERVICE_SCRIPT}
echo "Started Docker containers for ${PRJ_NAME} service."

if [ $? -ne 0 ]; then
  echo "ERROR: ${PRJ_NAME} not started."
  exit 1
fi

echo "---------------------------------------------------------"
echo "UAT instance $UAT_INSTANCE_ID started.    "
echo "---------------------------------------------------------"
set +x
set +e
