#!/bin/bash
#

CURR_DIR=`pwd`
if [ -z ${PRJ_NAME} ]; then
  echo "PRJ_NAME not set."
  exit 1
fi
echo "Building ${PRJ_NAME} ..."

cp $UAT_HOME/resources/${PRJ_NAME}/application-uat.properties $UAT_HOME/wrkspc/${PRJ_NAME}/src/main/resources/application-uat.properties 
cd $UAT_HOME/wrkspc/${PRJ_NAME}/
rm -rf ./build
./build.sh
cp $UAT_HOME/wrkspc/${PRJ_NAME}/build/libs/gandalf-1.0.0-SNAPSHOT.jar $UAT_HOME/docker/${PRJ_NAME}/images/appsrv/artifacts/gandalf-1.0.0-SNAPSHOT.jar

cd $CURR_DIR
echo "${PRJ_NAME} built OK."
