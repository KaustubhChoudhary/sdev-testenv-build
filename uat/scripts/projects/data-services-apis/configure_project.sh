#!/bin/bash
CURR_DIR=`pwd`

if [ -z ${PRJ_NAME} ]; then
  echo "PRJ_NAME not set."
  exit 1
fi

echo "Configuring ${PRJ_NAME} ..."

# Apply GIT patch
cd $UAT_HOME/wrkspc/${PRJ_NAME}
git apply $UAT_HOME/resources/${PRJ_NAME}/workspace.patch
if [ $? -ne 0 ]; then
   echo "ERROR: Unable to patch workspace."
   exit 1
fi

# Copy UAT properties
cp $UAT_HOME/resources/${PRJ_NAME}/uat.properties $UAT_HOME/wrkspc/${PRJ_NAME}/src/main/webapp/WEB-INF/properties/uat.properties
cd $CURR_DIR
echo "${PRJ_NAME} configured OK."
