#!/bin/bash
CURR_DIR=`pwd`

if [ -z ${PRJ_NAME} ]; then
  echo "PRJ_NAME not set."
  exit 1
fi

echo "Building ${PRJ_NAME} ..."

cp $UAT_HOME/resources/${PRJ_NAME}/uat.properties $UAT_HOME/wrkspc/${PRJ_NAME}/src/main/webapp/WEB-INF/properties/uat.properties
cd $UAT_HOME/wrkspc/${PRJ_NAME}/
rm -rf ./build
gradle clean  build -x test

#TODO: Check build status
# Copy build artifacts
cp $UAT_HOME/wrkspc/${PRJ_NAME}/build/libs/api-server-1.0.0-SNAPSHOT.war $UAT_HOME/docker/${PRJ_NAME}/images/appsrv/artifacts/api-server-1.0.0-SNAPSHOT.war

cd $CURR_DIR
echo "${PRJ_NAME} built OK."
