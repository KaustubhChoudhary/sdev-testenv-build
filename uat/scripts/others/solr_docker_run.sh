#!/bin/bash

CONT_NAME=test_solr
DATA_HOME=$UAT_HOME/data/solr/$CONT_NAME

if [ ! -d $DATA_HOME ]; then
  mkdir -p $DATA_HOME 2>&1 >> /dev/null
fi

docker run \
  --detach \
  --name=$CONT_NAME \
  --publish 8983:8983 \
  --volume=$DATA_HOME:/opt/solr/mydata \
 solr

RC=$?
if [ $RC -eq 0 ]; then
  echo "$CONT_NAME launched OK."
else
  echo "ERROR: Error running container $CONT_NAME"
  exit 1
fi

sleep 5 

docker exec -it --user=solr test_solr bin/solr create_core -c core_listing
docker exec -it --user=solr test_solr bin/solr create_core -c core_search
docker exec -it --user=solr test_solr bin/solr create_core -c internal_search
docker exec -it --user=solr test_solr bin/solr create_core -c polygon
docker exec -it --user=solr test_solr bin/solr create_core -c tags
docker exec -it --user=solr test_solr bin/solr create_core -c tag_search

echo "Configured container OK"

