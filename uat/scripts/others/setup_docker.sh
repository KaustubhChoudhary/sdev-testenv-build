#!/bin/sh
#
# Install docker CE on Ubuntu 16
#

# Adding repo
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

# Update and install
sudo apt-get -y update
apt-cache policy docker-ce
sudo apt-get install -y docker-ce
sudo systemctl status docker
echo "Installed docker OK."

# Add user
sudo usermod -aG docker ${USER}
echo "Added user ${USER} to docker group"

echo "Done."
