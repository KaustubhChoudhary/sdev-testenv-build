#!/bin/bash

CONT_NAME=test_mysql
DATA_HOME=$UAT_HOME/data/mysql/$CONT_NAME

if [ ! -d $DATA_HOME ]; then
  mkdir -p $DATA_HOME 2>&1 >> /dev/null
fi

docker run \
  --detach \
  --name=$CONT_NAME \
  --publish 3307:3306 \
  --env="MYSQL_ROOT_PASSWORD=notarealpassword" \
  --volume=$DATA_HOME:/var/lib/mysql \
  mysql
RC=$?
if [ $RC -eq 0 ]; then
  echo "$CONT_NAME launched OK."
else
  echo "ERROR: Error running container $CONT_NAME"
fi


# TODO: Pass in mysql config
# --volume=$DOCKER_TESTENV_HOME/config/mysql/test-mysql/conf.d:/etc/mysql/conf.d \
