#!/bin/bash

DOCKER_TESTENV_HOME=$HOME/docker/testenv

CONT_NAME=test_redis


docker run \
  --detach \
  --name=$CONT_NAME \
  --publish 6380:6379 \
  redis

echo "$CONT_NAME launched OK."
