#!/bin/bash
#
# Clones all the Dev repositories into your workspace.
#

BB_ACC_NAME=$1
WRKSPC_DIR=$2

if [ -z $BB_ACC_NAME ]; then
  echo "Please provide the Git username."
  exit 1
fi

if [ -z $WRKSPC_DIR ]; then
  echo "Please provide the workspace directory as an input"
  exit 1
fi

if [ -d $WRKSPC_DIR ]; then
  echo "Workspace directory [$WRKSPC_DIR] already exists. Please provide a new one"
  exit 1
fi


curr_dir=`pwd`
mkdir -p $WRKSPC_DIR
cd $WRKSPC_DIR


# Setting up repositories list
REPO_LIST=(
  'gandalf'
  'data-services-apis'
)

git config --global credential.helper 'cache --timeout=3600'

for repo in "${REPO_LIST[@]}"
do
  echo "[BEGIN] Cloning ${repo}.git into $WRKSPC_DIR"
  git clone https://$BB_ACC_NAME@bitbucket.org/swigy/${repo}.git
  echo "[END] Cloning ${repo}.git into $WRKSPC_DIR"
done

git config --global credential.helper 'cache --timeout=60'

cd $curr_dir
