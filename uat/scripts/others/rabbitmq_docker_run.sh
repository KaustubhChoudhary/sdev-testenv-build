#!/bin/bash

DOCKER_TESTENV_HOME=$HOME/docker/testenv
CONT_NAME=test_mq

docker run \
  --detach \
  --hostname my-rabbit \
  --name=$CONT_NAME \
  --env="RABBITMQ_DEFAULT_USER=user" \
  --env="RABBITMQ_DEFAULT_PASS=password" \
  --publish 8080:15672 \
  rabbitmq:3-management


echo "$CONT_NAME launched OK."
