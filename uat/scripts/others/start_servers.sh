#!/bin/bash


#USE_CLEAN_BUILD=$1
#if [ !-z $USE_CLEAN_BUILD ] && [ ${USE_CLEAN_BUILD} == "--clean" ]; then
#   USE_CLEAN_BUILD=1
#   echo "Not implemented : --clean"
#   exit 1
#else
#   USE_CLEAN_BUILD=0
#fi
#OPTS="clean"

TOMCAT_DIR=$UAT_HOME/staging/tomcat-7
WRKSPC_DIR=$UAT_HOME/wrkspc
CURR_DIR=`pwd`

#######################
## 1. Data-services
#######################
unzip -d $UAT_HOME/staging $UAT_HOME/lib/apache-tomcat-7.0.81.zip
mv $UAT_HOME/staging/apache-tomcat-7.0.81 $UAT_HOME/staging/tomcat-7
chmod +x $UAT_HOME/staging/tomcat-7/bin/*.sh

if [ ! -d /var/log/swiggy-dataservice ]; then
  mkdir /var/log/swiggy-dataservice
fi

cd $WRKSPC_DIR/data-services-apis

gradle $OPTS build -x test && \
 $TOMCAT_DIR/bin/shutdown.sh 2>&1 >> /dev/null && \
 rm -rf $TOMCAT_DIR/webapps/* && \
 cp build/libs/api-server-1.0.0-SNAPSHOT.war $TOMCAT_DIR/webapps/ROOT.war && \
 chmod 777 -R $TOMCAT_DIR/webapps && \
 $TOMCAT_DIR/bin/startup.sh

echo "Data Services API started OK."
exit 0

####################
## 2. Gandalf
####################
if [ ! -d /var/log/gandalf ]; then
  mkdir /var/log/gandalf
fi

cd $WRKSPC_DIR/gandalf
./build.sh && \
  java -jar -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5005 \
      build/libs/gandalf-1.0.0-SNAPSHOT.jar \
      --spring.profiles.active=uat 2>&1 > /dev/null

echo "Gandalf started OK"


cd $CURR_DIR
echo "All servers started."
