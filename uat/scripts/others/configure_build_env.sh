#!/bin/sh
#

echo "Installing JDK 8 ..."
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get -y update
sudo apt-get -y install oracle-java8-installer


echo "Setting up gradle ..."
unzip -d $UAT_HOME/staging/gradle/ $UAT_HOME/lib/gradle-2.14.1-bin.zip

echo "Done"

