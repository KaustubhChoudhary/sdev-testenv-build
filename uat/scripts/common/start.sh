#!/bin/bash
CURR_DIR=`pwd`

if [ -z ${PRJ_NAME} ]; then
  echo "PRJ_NAME not set."
  exit 1
fi
echo "Starting ${PRJ_NAME} service..."

# Create docker image and fire up the service
cd $UAT_HOME/docker/${PRJ_NAME}
docker-compose up --no-build -d

cd $CURR_DIR
#TODO: Check status
echo "Service started OK."
