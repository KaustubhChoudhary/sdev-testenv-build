#!/bin/bash
#
# Clones all the Dev repositories into your workspace.
# NOTE: Will delete any existing sources in the workspace.
# Full clone followed by branch change.
#

BB_ACC_NAME=$1
BB_PRJ_NAME=$2
BB_PRJ_BRANCH_NAME=$3
WRKSPC_DIR="$UAT_HOME/wrkspc"

if [ -z $BB_ACC_NAME ]; then
  echo "Please provide the Git username."
  exit 1
fi

if [ -z $BB_PRJ_NAME ]; then
  echo "Please provide the project name."
  exit 1
fi

if [ -z $BB_PRJ_BRANCH_NAME ]; then
  echo "No branch provided. Working with master."
fi
echo -e "---- Bit bucket account name : ${BB_ACC_NAME}\nProject name : ${BB_PRJ_NAME}\nbranch : ${BB_PRJ_BRANCH_NAME}\nWorkspace : ${WRKSPC_DIR}"

function finish {
  RC=$?
  if [ $RC -ne 0 ]; then
    echo "WARNING: Removing project directory as there were errors during setup."
    rm -rf "${WRKSPC_DIR}/${BB_PRJ_NAME}"
  fi
}
trap finish INT TERM EXIT



curr_dir=`pwd`

if [ -d  "${WRKSPC_DIR}/${BB_PRJ_NAME}" ]; then
  echo "Workspace already present. Deleting it and recreating."
  rm -rf "${WRKSPC_DIR}/${BB_PRJ_NAME}"
fi
cd ${WRKSPC_DIR}

git config --global credential.helper 'cache --timeout=3600'

repo="${BB_PRJ_NAME}"
echo "Cloning ${repo}.git into ${WRKSPC_DIR}/${BB_PRJ_NAME} .. "
git clone https://$BB_ACC_NAME@bitbucket.org/swigy/${repo}.git
echo "Cloned ${repo}.git into ${WRKSPC_DIR}/${BB_PRJ_NAME} OK."


cd ${WRKSPC_DIR}/${BB_PRJ_NAME}
if [ ! -z $BB_PRJ_BRANCH_NAME ]; then
  echo "Pointing to ${BB_PRJ_BRANCH_NAME} branch"
  git fetch && git checkout $BB_PRJ_BRANCH_NAME

  if [ "${BB_PRJ_BRANCH_NAME}" != "$(basename $(git symbolic-ref HEAD))" ]; then
    echo "ERROR: Branch does not match!"
    exit 1
  fi
fi

git diff --exit-code 2>&1 >> /dev/null
if [ $? -ne 0 ]; then
  echo "ERROR: Workspace is not clean!!. Exiting."
  exit 1
fi

git config --global credential.helper 'cache --timeout=60'
cd $curr_dir
