#!/bin/bash
CURR_DIR=`pwd`

if [ -z ${PRJ_NAME} ]; then
  echo "PRJ_NAME not set."
  exit 1
fi
echo "Building ${PRJ_NAME} docker image..."


#####
# Configuration for mysql : if mysql container is in dependency
####
if [ -d "${UAT_HOME}/docker/${PRJ_NAME}/images/mysql" ]; then
  # Creating data dir 
  DATA_DIR="${UAT_HOME}/data/mysql/${UAT_INSTANCE_ID}"

  if [ ! -d "${DATA_DIR}" ]; then
     mkdir ${DATA_DIR}
  else
     echo "MySql data directory already exists - ${DATA_DIR}"
  fi
  #TODO: By default do not reuse - Overriden with a force-use option

  # Preparing sqldump
  cp ${UAT_HOME}/resources/${PRJ_NAME}/*.sql ${UAT_HOME}/docker/${PRJ_NAME}/images/mysql/sqldump/
  chmod -R 777 ${UAT_HOME}/docker/${PRJ_NAME}/images/mysql/sqldump
fi

# Create docker image
cd $UAT_HOME/docker/${PRJ_NAME}
docker-compose create --build --force-recreate

cd $CURR_DIR
echo "Docker images built for ${PRJ_NAME} OK."
