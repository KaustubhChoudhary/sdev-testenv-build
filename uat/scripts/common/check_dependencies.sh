#!/bin/sh

# Check for Java
JAVA_VER=$(java -version 2>&1 | sed -n ';s/.* version "\(.*\)\.\(.*\)\..*"/\1\2/p;')
java -version
if [ "$JAVA_VER" -lt 18 ]; then
  echo "ERROR: Java version should be 1.8 or newer"
  exit 1
fi
echo "Java is installed OK."

# Check for gradle
gradle -v
if [ $? -ne 0 ]; then
  echo "ERROR: Gradle installation not found."
  exit 1
fi
echo "Gradle is installed OK."

# Check for docker
docker -v
if [ $? -ne 0 ]; then
  echo "ERROR: Docker installation not found."
  exit 1
fi
docker version
if [ $? -ne 0 ]; then
  user=$(whoami)
  echo "ERROR: Docker permissions not set for user = $user."
  exit 1
fi
echo "Docker is installed OK."

echo "All common UAT dependencies are resolved."
