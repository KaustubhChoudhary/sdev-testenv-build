TODO:

1. Wrapper script
   with commands to :
      setup - always a clean build, unpack the uat.tgz into new directory 
      start
      stop
      teardown

   - Impl locking

   - Impl registry

   - Versioning

=========================
How to setup
=========================


1. Unpack the archive ( uat.tgz ) in a clean directory.

    mkdir uat_1
    tar xvfz uat.tgz -C uat_1/



2. Build the uat environment passing the following :
     
UAT specific config :
	UAT_HOME
	APP_SRV_PORT
	UAT_TAG

BitBucket specific config :
	PRJ_NAME
	BRANCH
	BB_USER



Eg: For building "Data Services APIs" project with branch preorder

export UAT_HOME="/home/ubuntu/tmp/KCUAT_2/uat_3/uat" && \
export APP_SRV_PORT=8087 && \
export UAT_TAG="latest" && \
export PRJ_NAME="data-services-apis" && \
export BRANCH="" && \
export BB_USER="KaustubhChoudhary" && \
$UAT_HOME/scripts/build.sh



